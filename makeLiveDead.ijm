/*
 * After staining a cell population with annexin V and PI,
 * apoptotic cells show green fluorescence,
 * dead cells show red and green fluorescence, 
 * and live cells show little or no fluorescence.
 *  
 */

imageName=getTitle();
run("Duplicate...", " ");
getDimensions(width, height, channels, slices, frames);
newImage("PI", "8-bit black", width, height, 1);

//-- process open image
run("Images to Stack", "name=Stack title=[] use");
run("Set Label...", "label=DAPI");
setSlice(2);
run("Set Label...", "label=Annexin");

//-- threshold slice
setSlice(2);
setOption("BlackBackground", true);
run("Convert to Mask", "method=Triangle background=Dark only black");
run("Watershed", "slice");

roiManager("reset");
run("Analyze Particles...", "size=10.00-Infinity show=Masks add in_situ slice");
roiManager("Show None");


n = roiManager('count');

//-- what fraction of cells should be annexin positive? Of those, what percentage should be PI positive?
AnnexinPositiveFrac=0.4;
PI_PositiveFrac=0.5;
//-- Intensity properties of positive cells
AnnexinIntSD=30;
AnnexinIntMean=150;
PIIntSD=30;
PIIntMean=150;

//-- loop through cells
for (i = 0; i < n; i++) {
	roiManager('select', i);
    if (random<AnnexinPositiveFrac) {
    	run("Set...", "value="+(random("gaussian")*AnnexinIntSD+AnnexinIntMean)+" slice");
    	
    	//-- Some cells that are annexin positive will also be PI positive
    	if (random<PI_PositiveFrac) {
    		setSlice(3);
    		run("Set...", "value="+(random("gaussian")*PIIntSD+PIIntMean)+" slice");
    		setSlice(2);
    	}
	} else {
		//-- Cells that are not annexin positive should be negative (not 255 from the analyze particles step)
    run("Set...", "value=0 slice");
	}
}
run("Select None");

//-- Blur and display the synthetic channels
for (i = 2; i < 4; i++) {
setSlice(i);
run("Gaussian Blur...", "sigma=2 slice" );
run("Add Specified Noise...", "slice standard=10");
}

//-- Calibrate
Stack.setXUnit("um");
run("Properties...", "channels=3 slices=1 frames=1 pixel_width=0.3157895 pixel_height=0.3157895 voxel_depth=0.3157895");

run("Make Composite", "display=Composite");
Stack.setChannel(1);
run("Blue");
Stack.setChannel(2);
run("Green");
Stack.setChannel(3);
run("Red");

run("OME-TIFF...", "save=C:/hardcoded/output/path/LiveDead/"+imageName+".ome.tiff export compression=Uncompressed");
close("*");

//-- note that the XML headers will need editing to include channel names and colour info.
//-- use BFTOOLS: https://docs.openmicroscopy.org/bio-formats/latest/users/comlinetools/index.html
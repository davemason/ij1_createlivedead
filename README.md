# Creating semi-synthetic Live/Dead image data #

A quick and dirty ImageJ macro script to take input DAPI data and create two further channels as psudo Annexin V and Propidium Iodide channels.

After staining a cell population with annexin V and PI, apoptotic cells show green fluorescence, dead cells show red and green fluorescence and live cells show little or no fluorescence.

### Input data ###

Developed using https://bbbc.broadinstitute.org/BBBC002 as an input.